#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace Utils {
    extern GLuint windowWidth;
    extern GLuint windowHeight;

    GLuint loadShaderFromFile(const char *filename, GLenum shaderType);

    void window_close_callback(GLFWwindow* window);
    void window_size_callback(GLFWwindow* window, int width, int height);

};

#endif
