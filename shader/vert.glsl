#version 330

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTexcoord;
layout(location = 2) in vec3 vNormal;

out vec3 fPosition;
out vec2 fTexcoord;
out vec3 fNormal;

uniform mat4 uMvpMatrix;

void main()
{
	fPosition = vPosition;
	fTexcoord = vTexcoord;
	fNormal = vNormal;
	gl_Position = uMvpMatrix * vec4(vPosition, 1.0);
}
