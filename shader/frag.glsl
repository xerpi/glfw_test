#version 330

in vec3 fPosition;
in vec2 fTexcoord;
in vec3 fNormal;

out vec4 outColor;

uniform vec3 uCameraPos;
uniform mat4 uMvMatrix;
uniform mat3 uNormalMatrix;
uniform sampler2D uTexUnit;

uniform struct {
	vec3 position;
	vec4 color;
	float shininess;
} uLightSource;

void main()
{
	vec3 normal = normalize(uNormalMatrix * fNormal);
	vec3 position = vec3(uMvMatrix * vec4(fPosition, 1.0f));

	vec3 L = uLightSource.position - position;

	float brightness = dot(L, normal)/(length(L) * length(normal));
	float diffuse = clamp(brightness, 0, 1);

	vec3 reflection = reflect(-L, normal);
	vec3 viewerDirection = uCameraPos - position;

	float specularFactor = dot(reflection, viewerDirection)/(length(reflection) * length(viewerDirection));
	float specular = pow(max(specularFactor, 0.0f), uLightSource.shininess);

	vec4 texColor = texture2D(uTexUnit, fTexcoord);
	outColor = vec4(vec3(uLightSource.color) * (diffuse + specular) * texColor.rgb, texColor.a);
}
