TARGET   = $(notdir $(CURDIR))
INCLUDES = include
SOURCES  = source
DATA     = data

CPPFILES := $(foreach dir,$(SOURCES),$(wildcard $(dir)/*.cpp))
CFILES   := $(foreach dir,$(DATA),$(wildcard $(dir)/*.c))
OBJS     := $(CPPFILES:.cpp=.o) $(CFILES:.c=.o)

CFLAGS   = -I$(INCLUDES)
CXXFLAGS = $(CFLAGS)

LIBS = -lglfw -lGLEW -lGL

all: $(TARGET)

$(TARGET): $(OBJS)
	g++ -o $(TARGET) $^ $(LIBS)

%.o: %.cpp
	g++ -c -o $@ $< $(CXXFLAGS)

%.o: %.c
	gcc -c -o $@ $< $(CFLAGS)

clean:
	rm -f $(TARGET) $(OBJS)

run: $(TARGET)
	./$(TARGET)
