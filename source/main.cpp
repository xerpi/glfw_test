#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Utils.hpp"
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <cstdio>
using namespace std;

extern "C" {
extern const struct {
	unsigned int 	 width;
	unsigned int 	 height;
	unsigned int 	 bytes_per_pixel;
	unsigned char	 pixel_data[];
} box_texture;
};

#define CUBE_SIZE 0.5f
static const float cube_vertices[][3] = {
	// Front
	-CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,

	// Right
	 CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,

	// Back
	 CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,

	// Left
	-CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,

	// Top
	-CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	 CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	-CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE,  CUBE_SIZE, -CUBE_SIZE,
	 CUBE_SIZE,  CUBE_SIZE,  CUBE_SIZE,

	// Bottom
	-CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
	-CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE,  CUBE_SIZE,
	 CUBE_SIZE, -CUBE_SIZE, -CUBE_SIZE,
};

static const float cube_texcoords[] = {
	0.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	0.0f, 0.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,

	0.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	0.0f, 0.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,

	0.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	0.0f, 0.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,

	0.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	0.0f, 0.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,

	0.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	0.0f, 0.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,

	0.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 0.0f,
	0.0f, 0.0f,
	1.0f, 1.0f,
	1.0f, 0.0f,
};

static const float cube_normals[] = {
	// Front
	 0.0f,  0.0f,  1.0f,
	 0.0f,  0.0f,  1.0f,
	 0.0f,  0.0f,  1.0f,
	 0.0f,  0.0f,  1.0f,
	 0.0f,  0.0f,  1.0f,
	 0.0f,  0.0f,  1.0f,

	// Right
	 1.0f,  0.0f,  0.0f,
	 1.0f,  0.0f,  0.0f,
	 1.0f,  0.0f,  0.0f,
	 1.0f,  0.0f,  0.0f,
	 1.0f,  0.0f,  0.0f,
	 1.0f,  0.0f,  0.0f,

	// Back
	 0.0f,  0.0f, -1.0f,
	 0.0f,  0.0f, -1.0f,
	 0.0f,  0.0f, -1.0f,
	 0.0f,  0.0f, -1.0f,
	 0.0f,  0.0f, -1.0f,
	 0.0f,  0.0f, -1.0f,

	// Left
	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,

	// Top
	 0.0f,  1.0f,  0.0f,
	 0.0f,  1.0f,  0.0f,
	 0.0f,  1.0f,  0.0f,
	 0.0f,  1.0f,  0.0f,
	 0.0f,  1.0f,  0.0f,
	 0.0f,  1.0f,  0.0f,

	// Bottom
	 0.0f, -1.0f,  0.0f,
	 0.0f, -1.0f,  0.0f,
	 0.0f, -1.0f,  0.0f,
	 0.0f, -1.0f,  0.0f,
	 0.0f, -1.0f,  0.0f,
	 0.0f, -1.0f,  0.0f,
};

unsigned int frames = 0;
double lastTime = 0.0, deltaTime = 0.0;
void calcFPS();

int main(void)
{
	GLFWwindow *window = NULL;
	if (!glfwInit())
		return -1;

	window = glfwCreateWindow(Utils::windowWidth, Utils::windowHeight, "Such OpenGL", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (glewInit() != GLEW_OK) {
		cout << "glewInit failed" << endl;
		glfwTerminate();
		return -1;
	}

	glfwSwapInterval(0);

	glfwSetWindowCloseCallback(window, Utils::window_close_callback);
	glfwSetWindowSizeCallback(window, Utils::window_size_callback);

	GLuint vertexShader = Utils::loadShaderFromFile("shader/vert.glsl", GL_VERTEX_SHADER);
	GLuint fragmentShader = Utils::loadShaderFromFile("shader/frag.glsl", GL_FRAGMENT_SHADER);

	GLuint program = glCreateProgram();

	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	glLinkProgram(program);

	glDetachShader(program, vertexShader);
	glDetachShader(program, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	GLuint cube_vert_VBO;
	glGenBuffers(1, &cube_vert_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, cube_vert_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GLuint cube_texcoord_VBO;
	glGenBuffers(1, &cube_texcoord_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, cube_texcoord_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cube_texcoords), cube_texcoords, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GLuint cube_norm_VBO;
	glGenBuffers(1, &cube_norm_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, cube_norm_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cube_normals), cube_normals, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glm::vec3 cameraPos(0.0f);
	glm::mat4 modelMatrix(1.0f);
	glm::mat4 viewMatrix(1.0f);
	glm::mat4 mvMatrix(1.0f);
	glm::mat4 mvpMatrix(1.0f);
	glm::mat3 normalMatrix(1.0f);

	const glm::mat4 projectionMatrix = glm::perspective(glm::radians(45.0f), Utils::windowWidth/float(Utils::windowHeight), 0.1f, 100.0f);

	GLint cameraPosLoc = glGetUniformLocation(program, "uCameraPos");
	GLint mvpMatrixLoc = glGetUniformLocation(program, "uMvpMatrix");
	GLint mvMatrixLoc = glGetUniformLocation(program, "uMvMatrix");
	GLint normalMatrixLoc = glGetUniformLocation(program, "uNormalMatrix");

	GLint lightSrcPosLoc = glGetUniformLocation(program, "uLightSource.position");
	GLint lightSrcColLoc = glGetUniformLocation(program, "uLightSource.color");
	GLint lightSrcShiLoc = glGetUniformLocation(program, "uLightSource.shininess");

	GLuint texture;
	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, box_texture.width, box_texture.height,
			0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, box_texture.pixel_data);
	glBindTexture(GL_TEXTURE_2D, 0);

	GLint texUnitLoc = glGetUniformLocation(program, "uTexture");

	GLuint VAO;
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, cube_vert_VBO);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), 0);
		glBindBuffer(GL_ARRAY_BUFFER, cube_texcoord_VBO);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), 0);
		glBindBuffer(GL_ARRAY_BUFFER, cube_norm_VBO);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
	glBindVertexArray(0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW);

	struct {
		glm::vec3 position;
		glm::vec4 color;
		float shininess;
	} lightSource;

	lightSource.position = glm::vec3(0.0f);
	lightSource.color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	lightSource.shininess = 5.0f;

	float rot_x = 0.0f, rot_y = 0.0f, rot_z = 0.0f;
	float trans_x = 0.0f, trans_y = 0.0f, trans_z = -5.0f;

	while (!glfwWindowShouldClose(window)) {
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			trans_x += 0.01f;
		else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			trans_x -= 0.01f;

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			trans_z -= 0.01f;
		else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			trans_z += 0.01f;

		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			trans_y += 0.01f;
		else if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			trans_y -= 0.01f;

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
			rot_y -= 0.01f;
		else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
			rot_y += 0.01f;

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
			rot_x -= 0.01f;
		else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
			rot_x += 0.01f;

		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
			rot_x = rot_y = rot_z = 0.0f;
			trans_x = trans_y = 0.0f;
			trans_z = -5.0f;
		}

		/*double x, y;
		glfwGetCursorPos(window, &x, &y);
		rot_x = float(x-Utils::windowWidth/2)/100.0f;
		rot_y = float(y-Utils::windowHeight/2)/100.0f;*/

		glm::mat4 trans = glm::translate(glm::vec3(trans_x, trans_y, trans_z));
		glm::mat4 rot(1.0f);
		rot = glm::rotate(rot, rot_x, glm::vec3(1.0f, 0.0f, 0.0f));
		rot = glm::rotate(rot, rot_y, glm::vec3(0.0f, 1.0f, 0.0f));
		rot = glm::rotate(rot, rot_z, glm::vec3(0.0f, 0.0f, 1.0f));

		viewMatrix = glm::translate(-cameraPos);

		modelMatrix = trans * rot;
		mvMatrix = viewMatrix * modelMatrix;
		mvpMatrix = projectionMatrix * mvMatrix;
		normalMatrix = glm::transpose(glm::inverse(glm::mat3(mvMatrix)));

		glUseProgram(program);
			glUniform3fv(cameraPosLoc, 1, glm::value_ptr(cameraPos));
			glUniformMatrix4fv(mvpMatrixLoc, 1, GL_FALSE, glm::value_ptr(mvpMatrix));
			glUniformMatrix4fv(mvMatrixLoc, 1, GL_FALSE, glm::value_ptr(mvMatrix));
			glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
			glUniform3fv(lightSrcPosLoc, 1, glm::value_ptr(lightSource.position));
			glUniform4fv(lightSrcColLoc, 1, glm::value_ptr(lightSource.color));
			glUniform1f(lightSrcShiLoc, lightSource.shininess);
			glUniform1i(texUnitLoc, 0);
			glBindVertexArray(VAO);
				glDrawArrays(GL_TRIANGLES, 0, 36);
			glBindVertexArray(0);
		glUseProgram(0);
		calcFPS();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteTextures(1, &texture);
	glDeleteBuffers(1, &cube_vert_VBO);
	glDeleteBuffers(1, &cube_texcoord_VBO);
	glDeleteBuffers(1, &cube_norm_VBO);
	glDeleteProgram(program);
	glfwTerminate();
	return 0;
}


void calcFPS()
{
	frames++;
	deltaTime = glfwGetTime() - lastTime;
	if (deltaTime >= 1.0) {
		cout << (frames/deltaTime) << endl;
		frames = 0;
		lastTime = glfwGetTime();
	}
}

