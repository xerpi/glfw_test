#include "Utils.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

GLuint Utils::windowWidth  = 720;
GLuint Utils::windowHeight = 480;

GLuint Utils::loadShaderFromFile(const char *filename, GLenum shaderType)
{
	std::ifstream ifs(filename);
	std::string sourceStr;
	if (not ifs.is_open()) {
		std::cout << "Could not open: " << filename << std::endl;
		return 0;
	}

	ifs.seekg(0, std::ios::end);
	sourceStr.reserve(ifs.tellg());
	ifs.seekg(0, std::ios::beg);

	sourceStr.assign((std::istreambuf_iterator<char>(ifs)),
					  std::istreambuf_iterator<char>());

	const GLchar *sourcePtr = sourceStr.c_str();
	GLuint shaderId = glCreateShader(shaderType);
	glShaderSource(shaderId, 1, &sourcePtr, NULL);
	glCompileShader(shaderId);

	GLint compileStatus;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compileStatus);

	if (compileStatus != GL_TRUE) {
		GLint infoLogLen;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLen);
		GLchar *infoLog = new GLchar[infoLogLen+1];
		glGetShaderInfoLog(shaderId, infoLogLen, NULL, infoLog);
		std::cout << "Error compiling " << filename << " shader:" << infoLog << std::endl;
		delete infoLog;
		glDeleteShader(shaderId);
		return 0;
	}

	return shaderId;
}


void Utils::window_close_callback(GLFWwindow* window)
{

}


void Utils::window_size_callback(GLFWwindow* window, int width, int height)
{
	windowWidth = width;
	windowHeight = height;
	glViewport(0, 0, width, height);
}

